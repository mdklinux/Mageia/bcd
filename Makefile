NAME=bcd
VERSION=3.7
PERL_VENDORLIB=$(shell eval "`perl -V:installvendorlib`"; echo $$installvendorlib)

ALL=
PM=Bcd.pm Common.pm Genisoimage.pm Isolinux.pm Media.pm Stagex.pm Resign.pm
POD=${name}.pod

all:	man tex pdf
	
clean:
	rm -rf *~
	rm -rf BCD/*~
	rm -rf lists/*~
	rm -rf doc/*~
	rm -rf *.1.xz
	cd doc && rm -rf *.toc *.out *.log *.aux

cleandist: clean
	rm -rf $(NAME)-$(VERSION) $(NAME)-$(VERSION).tar.bz2

install: $(ALL) man
	install -d $(PREFIX)/usr/bin
	install bcd.pl $(PREFIX)/usr/bin/bcd
	install -m755 check_sign_by_path.pl $(PREFIX)/usr/bin/
	install -m755 resign_rpm_by_path.pl $(PREFIX)/usr/bin/
	install -d $(PREFIX)/$(PERL_VENDORLIB)/BCD
	install BCD/*.pm $(PREFIX)/$(PERL_VENDORLIB)/BCD
	install doc/${NAME}.pod $(PREFIX)/$(PERL_VENDORLIB)
	install -d $(PREFIX)/usr/share/man/man1
	install doc/${NAME}.1.xz $(PREFIX)/usr/share/man/man1/
	install doc/conf.xml.1.xz $(PREFIX)/usr/share/man/man1/

man:
	pod2man doc/${NAME}.pod > doc/${NAME}.1
	pod2man doc/conf.xml.pod > doc/conf.xml.1
	xz -f doc/${NAME}.1
	xz -f doc/conf.xml.1

tex:
	cd doc && pod2latex ${NAME}.pod

pdf:	
	cd doc && latex2pdf howto_bcd.tex

tar:
	git archive --prefix=$(NAME)-$(VERSION)/ HEAD | xz  > $(NAME)-$(VERSION).tar.xz;

dist: cleandist tar
