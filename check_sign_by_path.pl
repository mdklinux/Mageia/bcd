#!/usr/bin/perl -w

use strict;
use Parallel::ForkManager;
use File::Glob ':glob';
use File::Basename;

#rpm --checksig -v ~/pieces/2010.1/i586/media/main/release/basesystem-2010.0-2mdv2010.1.i586.rpm 
#    Header V3 DSA signature: OK, key ID 26752624
#    Header SHA1 digest: OK (fdeb18c37115229582c4b42dba7d834c7b82bc8c)
#    MD5 digest: OK (74cee8f749ae4443cb6dae3b65cc5a6e)
#    V3 DSA signature: OK, key ID 26752624

#gpg -a pubkey_main 
#pub  1024D/26752624 2003-12-10 MandrakeCooker <cooker@linux-mandrake.com>
#sub  1024g/E5CC3CAA 2003-12-10

my $NB_FORK=15;
my $path = $ARGV[0];
#my $key = $ARGV[1];

$ARGV[0] or die "First arg must be a path to rpm\n";
#$ARGV[1] or die "Second arg must be a the key (ie: 26752624)\n";

my $B;
my $convert = $path;
$convert =~ tr|\/|_|;
my $log = "log_check_sign_$convert.log";
system("cp /dev/null -f $log");
my $pm = new Parallel::ForkManager($NB_FORK);
my @list_pkg = glob("$path/*.rpm");
my $count = @list_pkg;
my $key = `gpg -a $path/media_info/pubkey | grep pub | cut -d "/" -f 2 | cut -d " " -f 1`;
print "$count transactions to do ... be patient !!!!\n";
my $status = "0";
open $B, ">>$log";
foreach my $pkg (@list_pkg) {
	$pkg or next;
	my $basename_pkg = basename($pkg);
        $status++;
        my $pid = $pm->start and next;
        print("$basename_pkg ($status/$count)\n");
	#my @command = `LC_ALL=C rpm --rcfile=$rpmrc --checksig -v $pkg | grep "key ID"`;
        my @command = `LC_ALL=C rpm --checksig -v $pkg | grep "key ID"`;
	foreach (@command) {
		if ($_ =~ /$key/i) {
			# dont print if it is OK
			# print "$pkg signature OK\n";
			last;
		} else {
			print "$pkg signature NOK\n";
			print $B "$pkg NOK\n";
			last;
		}
		next;
	}
        $pm->finish;
}
#print "Waiting for the end of some check...\n";
$pm->wait_all_children;
close $B;
#print "all check are done...\n";
