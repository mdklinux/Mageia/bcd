#!/bin/bash

arch=$1
distrib=Mageia
version=6
homebcd=/home/bcd/build_bcd
bin=$homebcd/bcd
pieces=$homebcd/pieces/iso
build=$homebcd/build/$distrib-$version-$arch

# (tmb) move symlink depending on arch
pushd /home/bcd/build_bcd/bcd/BCD
if [ "$arch" = "x86_64" ]
then	
	ln -sf Genisoimage.pm.x86_64 Genisoimage.pm
else
	ln -sf Genisoimage.pm.i586 Genisoimage.pm
fi
popd

$bin/bcd.pl dvd_free-$arch.xml all noiso

#rm -rf $build/$arch/isolinux
#cp -r $pieces/dvd/isolinux $build
mv $homebcd/build/$distrib-$version-$arch/$arch/isolinux $homebcd/build/$distrib-$version-$arch
cp $pieces/dvd/$arch.cfg $build/isolinux/isolinux.cfg
cp $pieces/dvd/tools.cfg $build/isolinux/

rm -f $build/isolinux/i386.cfg

rm -f $build/$arch/install/images/*nonfree*

if [ "$arch" = "x86_64" ]
then	
	cd $build
	tar -xvJf $pieces/DVD-EFI.tar.xz
	cd -
	rm -f $build/isolinux/$arch/all-nonfree.rdz
else
	rm -f $build/isolinux/i386.cfg
	perl -pi -e 's/i586/i386/' $build/isolinux/isolinux.cfg
	rm -f $build/isolinux/i386/all-nonfree.rdz
fi

cp -f $pieces/message $build/isolinux/bootlogo

rm -rf $homebcd/build/$distrib-$version-$arch/$arch/install/images

$bin/bcd.pl dvd_free-$arch.xml iso
date > $homebcd/iso/$distrib-$version-$arch/DATE.txt
cd $homebcd/iso/$distrib-$version-$arch

for i in md5 sha1 sha512
do
${i}sum $distrib-$version-$arch-DVD.iso > $distrib-$version-$arch-DVD.iso.$i
done
