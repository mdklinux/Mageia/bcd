package BCD::Web;

use strict;
use BCD::Genisoimage qw($isoname);
use URPM;
use File::Slurp;
use MDK::Common;

use BCD::Common qw(:DEFAULT $isoconf $isodir $wd $arch $version $based_on $repo $THEME $name $conf_file $date $urpmqlogs $rpmcheck_list_pkg $file_rpmcheck_log $smartcheck_list_pkg $file_smartcheck_log);

our @ISA = qw(Exporter);
our @EXPORT = qw(main_html);

my $verbose;
my $LOG="MEDIA -";
my $color = "pink";

my $index = $isoconf->{nameid} . '-' . $isoconf->{mediainfo}{version} . '-' . $isoconf->{arch} . ".html";
my $public_html = "~/public_html";
my $webdir = "$public_html/$isoname-$date";

map { !-d "$webdir/$_" and system("mkdir -p $webdir/$_") } qw(log rpmcheck input iso);

sub main_html {
	#next_build_info;
	show_general_info();
	copy_conf();
	link_input();
	prep_build_log();
	show_rpmcheck();
	rename_iso();
	link_urpmq_log();
	#clean_previous_iso();
	link_iso();
	open my $B, ">>$index";
	print $B "</html>";
	close $B;
	system("mv $index $webdir/");
	system("ln -sf $webdir/$index $webdir/index.html");
}

sub show_general_info {
	my $date_repo;
	if (-f "$repo/$based_on/$arch/VERSION") {
		$date_repo = read_file("$repo/$based_on/$arch/VERSION");
	} else { $date_repo = "UNKNOW date"; print "You should check that a VERSION file is present" }
	open my $B, ">$index";
	print $B "<HTML>
	<HEAD>
	<STYLE TYPE='text/css'>
a:link {
    color: #0073c7;
    background-color: inherit;
}

a:visited {
    color: #5A88B5;
    background-color: inherit;
}

a:hover,
a:active {
    color: #0073c7;
    background-color: #f0f0f0;
}
h2 {
    padding: 0.2em;
    margin: 0 0 0.5em 0;
    border: 1px solid #405871;
    background-color: inherit;
    color: #036;
    text-decoration: none;
    font-size: 22px;
    font-weight: bold;
}

h1 {
    padding: 0.2em 0 0.2em 0.7em;
    margin: 10px 0 0.5em 0;
    text-decoration: none;
    font-size: 18px;
    font-weight: bold;
    background: #FF7700;
	color: white;
}
h3 {
    background-color: inherit;
    color: #ff7700;
    text-decoration: none;
    font-weight: bold;
    font-size: 16px;
    margin: 1.3em 0 0.4em 0;
    padding: 0;
}

	</STYLE>
	</HEAD>
	<TITLE>$isoname</TITLE>
	<h1 name='main h1'>$isoname build at $date</h1>
	<BODY>
	";
	print $B "<ul>\n";
	print $B "<li>Date of the synchronisation to the cooker repository: <b>$date_repo</b></li>\n";
	print $B "<li>Configuration file used: <a href='$conf_file'>$conf_file</a></li>\n";
	print $B "<li>Architecture: <b>$arch</b></li>\n";
	print $B "<li>Distribution based on: <b>$based_on</b></li>\n";
	print $B "<li>Version: <b>$version</b></li>\n";
	print $B "<li>Theme used: <b>$THEME</b></li>\n";
	print $B "</ul>\n";
	close $B;
}

sub copy_conf {
	system("cp $conf_file $webdir/");
}


sub link_input {
    open my $B, ">>$index";
    print $B "<hr>\n";
    print $B "<h1 name='Media list'>Media list</h1>\n";
    close $B;
    foreach my $media (@{$isoconf->{media}{list}}) {
    $media->{name} or next;
    open my $B, ">>$index";
    print $B "<h2 name='Media $media->{name}'>Media $media->{name}</h1>";
    print $B "<pre>
Name of the media: $media->{name}
Description: $media->{desc}
From wich dir: $media->{mediadir}
destination dir: $media->{destmedia}
Pubkey: $media->{pubkey}
</pre>
";
    	print $B "<h3 name='list of input file'>list of input file</h3>\n";
	print $B "<ul>\n";
        foreach my $files (@{$media->{input}}) {
                $files->{file} or next;
                if (-f $files->{file}) {
		my $short_name = basename($files->{file});
		print $B "<li><a href='input/$short_name'>$short_name</a></li>\n";
		system("cp -af $files->{file} $webdir/input/");
                } else { print $B "<li><b>$files->{file} not present</b></li>" }

        }
	print $B "</ul>\n";
	print $B "<h3 name='list of exclude file'>list of exclude file</h3>\n";
	print $B "<ul>\n";
        foreach my $files (@{$media->{exclude}}) {
                $files->{file} or next;
                if (-f $files->{file}) {
		my $short_name = basename($files->{file});
		print $B "<li><a href='input/$short_name'>$short_name</a></li>\n";
		system("cp -af $files->{file} $webdir/input/");
                } else { print $B "</li><b>$files->{file} not present</b></li>" }
        }
	print $B "</ul>\n";
   close $B;
   }
}

sub prep_build_log {
	system("cp -af $urpmqlogs/$name-$version-$arch/* $webdir/log/");
}

sub show_rpmcheck {
	my $size;
	system("cp -af $rpmcheck_list_pkg $file_rpmcheck_log $webdir/rpmcheck/");
	open my $B, ">>$index";
	print $B "<h1 name='RPMCHECK status'>RPMCHECK Status</h1>\n";
	my @files = glob("$webdir/rpmcheck/*");
	foreach (@files) {
	       	$size = (stat($_))[7];
		print $size;
		if ($size ne 0) {
			print $B "<fond color=red><b>ERROR in RPMCHECK !</b></font>\n";
		}
	}
	print $B "<a href=rpmcheck/>rpmcheck directory</a> (rpmcheck --explain --failures)\n";
	close $B;
}

sub clean_previous_iso {
	system("find $public_html/$isoname* -name \.iso -exec rm -vf {} \;");
}

sub rename_iso {
	#-f "$isodir/$isoname.iso" and system("cp -f $isodir/$isoname.iso $webdir/iso/$isoname-$date.iso");
	-f "$isodir/$isoname.idx" and system("cp -f $isodir/$isoname.idx $webdir/iso/$isoname-$date.idx");
	-f "$isodir/$isoname.iso.md5" and system("cp -f $isodir/$isoname.iso.md5 $webdir/iso/$isoname-$date.iso.md5");
	-f "$isodir/$isoname.iso.sha1" and system("cp -f $isodir/$isoname.iso.sha1 $webdir/iso/$isoname-$date.iso.sha1");

}

sub link_urpmq_log {
	open my $B, ">>$index";
	print $B "<hr>\n";
	print $B "<h1 name='Link to URPMQ logs'>Link to URPMQ logs</h1>\n";
	print $B "<a href=log/>All log of URPMQ command per package/media</a> (based on: urpmq --requires-recursive -c --sources)\n";
	close $B;
}

sub link_iso {
	open my $B, ">>$index";
    	print $B "<hr>\n";
	print $B "<h1 name='Link to ISO, IDX, SHA1 and MD5'>Link to ISO, IDX, SHA1 and MD5</h1>\n";
	print $B "<ul>\n";
	if (-f "$webdir/iso/$isoname-$date.iso") {
		print $B "<li><a href=iso/$isoname-$date.iso>$isoname-$date.iso</a></li>\n";
		print $B "<li><a href=iso/$isoname-$date.iso.md5>$isoname-$date.iso.md5</a></li>\n";
		print $B "<li><a href=iso/$isoname-$date.iso.sha1>$isoname-$date.iso.sha1</a></li>\n";
	} else { 
		print $B "<li><fond color=red><b>No ISO available</b></font></li>\n";
	}
	print $B "<li><a href=iso/$isoname-$date.idx>$isoname-$date.idx</a></li>\n";
	print $B "</ul>\n";
	close $B;
}


1;
