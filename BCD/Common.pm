package BCD::Common;

use strict;
use XML::Simple;
use YAML;
use MDK::Common;
use Term::ANSIColor;
use File::Copy::Recursive qw(dircopy pathrm);
use File::Glob ':glob';

use BCD::Bcd qw(:DEFAULT);

our @ISA = qw(Exporter);
our @EXPORT = qw(create_needed_dir check_dir clean_all print_color);
# export all needed var
our @EXPORT_OK = qw($nameid $isoconf $wd $name $arch $file $based_on $version $repo $isodir $builddir $distrib_path $error_color $DISTRIB $THEME $tmp_rpmsrate $NB_FORK $dir_deps @list_of_removed $rpmrc $pwd_file $conf_file $date $urpmqlogs $rpmcheck_list_pkg $file_rpmcheck_log $smartcheck_list_pkg $smartcheck_dir $file_smartcheck_log $chroot_path $support);

my $LOG="COMMON -";
my $color = "red";


our @list_of_removed;

my ($sec, $min, $hour, $mday, $mon, $year, undef, undef, undef) = localtime(time());
$year = $year + 1900; $mon = $mon + 1;
our $date = $year . '_' . $mon . '_' . $mday . '-' . $hour . 'h' . $min . '-' . $sec;

sub whereisthedoc { 
	print_color(" 'man bcd' and man 'conf.xml'\nREAD the full documentation in pdf format available in the /usr/share/doc/bcd dir",$color) and exit 1;
}
if (!$ARGV[0]) {
       print_color(" First arg must be the XML file to use!", $color);
       whereisthedoc;
} elsif (!$ARGV[1]) {
	print_color(" You must do an action ! actions are:", $color);
	print_color("###\nstagex isolinux clean media gendistrib rpmcheck list mediarepo checkrepo all\nmd5 resign clean doble nodoble kernel info iso verbose\n###", $color);
	whereisthedoc;
}

our $conf_file = $ARGV[0];
our $isoconf;

if ($conf_file =~ /\.xml$/i) {
  $isoconf = XMLin($conf_file, keyattr => ['']);
} elsif ($conf_file =~ /\.yaml$/i) {
  $isoconf = YAML::LoadFile($conf_file);
} else {
  print_color("You must provide an XML or YAML config file", $color) and exit 1;
}

our $DISTRIB = $isoconf->{theme}{name};
our $THEME = $isoconf->{theme}{theme};
our $name = $isoconf->{nameid};
our $NB_FORK = $isoconf->{nb_fork};
our $wd = $isoconf->{workdir}{path};
our $arch = $isoconf->{arch};
our $version = $isoconf->{mediainfo}{version};
our $repo = $isoconf->{repo}{path};
our $based_on = $isoconf->{based_on};

$DISTRIB or print_color("You must define the <theme><name> tag",$color) and exit 1;
$THEME or print_color("You must define the <theme><theme> tag",$color) and exit 1;
$name or print_color("You must define the <namedid> tag",$color) and exit 1;
$wd or print_color("You must define the <workdir><path> tag",$color) and exit 1;
$arch or print_color("You must define the <arch> tag",$color) and exit 1;
$NB_FORK or print_color("You must define the <nb_fork> tag",$color) and exit 1;
$version or print_color("You must define the <mediainfo><version>", $color) and exit 1;
$repo or print_color("You must define the <repo><path> tag",$color) and exit 1;
$based_on or print_color("You must define the <based_on> tag",$color) and exit 1;
$isoconf->{chroot_media} or print_color("Now you must define <chroot_media> in the conf.xml file",$color) and exit 1;
$isoconf->{chroot_media_exclude} or print_color("Now you must define <chroot_media_exclude> in the conf.xml file",$color) and exit 1;

our $chroot_path = "/tmp/$isoconf->{nameid}-$version-$isoconf->{arch}";

our $rpmcheck_list_pkg = "/tmp/rpmcheck_list_pkg_to_add_$isoconf->{nameid}-$version-$isoconf->{arch}";
our $file_rpmcheck_log = "/tmp/rpmcheck_failure_$isoconf->{nameid}-$version-$isoconf->{arch}";
our $smartcheck_list_pkg = "/tmp/smartcheck_list_pkg_to_add_$isoconf->{nameid}-$version-$isoconf->{arch}";
our $file_smartcheck_log = "/tmp/smartcheck_failure_$isoconf->{nameid}-$version-$isoconf->{arch}";
our $smartcheck_dir = "/tmp/smartcheck_dir_$isoconf->{nameid}-$version-$isoconf->{arch}";

if (!defined($isoconf->{media}{first_media})) {
	print_color("$LOG Please in the media list inform about the first_media name (<first_media>Main</first_media>)", $color); 
	whereisthedoc;
}
if (!defined($isoconf->{media}{update_prefix})) {
	print_color("$LOG Please in the media list inform about the prefix use to define a media as an update, ie: 'Updates' (<update_prefix>Updates</update_prefix>)", $color);
	whereisthedoc;
}

# password file
#"/home/plop/.signature.gpg";
our $pwd_file = $isoconf->{signature};
#"/home/plop/.rpmrc";
our $rpmrc = $isoconf->{rpmrc};

# where to store urpmq queries results
my $urpmqlogs;
$isoconf->{urpmqlogs} and $urpmqlogs=$isoconf->{urpmqlogs} or $urpmqlogs="/var/lib/bcd/";
create_needed_dir($urpmqlogs);
our $dir_deps = "$urpmqlogs/$name-$version-$arch";
our $builddir = "$wd/$isoconf->{iso}{genisoimage}{builddir}/$name-$version-$arch/$arch";
our $isodir = "$wd/$isoconf->{iso}{genisoimage}{isodir}/$name-$version-$arch";
# distrib path in chroot (mounted loop)
our $distrib_path = "/tmp/distrib";
our $tmp_rpmsrate = "/tmp/rpmsrate_$name-$version-$arch";
our $error_color = "blue on_magenta";

sub print_color {
	my ($text, $color) = @_;
	print color $color;
	print "$text\n";
	print color 'reset';
	
}

sub create_needed_dir {
	my ($dir) = @_;
        # create needed dir to create the build tree, and to store the ISO
	if (! -d $dir) {
		#mkdir $dir, 0755;
		print_color("$LOG creation of $dir", $color);
		mkdir_p($dir);
	} else {
		print_color("$LOG $dir already exist", $color);
	}
}

sub check_dir {
	create_needed_dir($builddir);
	create_needed_dir("$builddir/$isoconf->{mediainfo}{mediainfo_dir}");
	create_needed_dir($isodir);
	create_needed_dir("/tmp/$isoconf->{nameid}-$version-$isoconf->{arch}");
}

sub clean_all {
	my @l = glob("/tmp/$isoconf->{nameid}-$version-$isoconf->{arch}/$distrib_path/*");
	foreach (@l) {
	    $_ or next;
	    if (-f "$_" || -d "$_") { 
		print_color("Distribution repositery already mounted exiting !\n Check /tmp/$isoconf->{nameid}-$version-$isoconf->{arch}/$distrib_path directory", $color);
		print_color("sudo umount /tmp/$isoconf->{nameid}-$version-$isoconf->{arch}$distrib_path", $error_color);
		foreach my $media (@{$isoconf->{media}{list}}) {
		    $media->{name} or next;
		    $media->{external}{name} and print_color("You should check also umount the external media /tmp/$isoconf->{nameid}-$version-$isoconf->{arch}/tmp/$media->{external}{name}\nsudo umount /tmp/$isoconf->{nameid}-$version-$isoconf->{arch}/tmp/$media->{external}{name}", $error_color);
		}
		print_color("WARNING: if you didnt umount repositery or external media you can erase them unless they are mounted bind in RO mode.", $color);
		exit 1;
		}
	}
	print_color("$LOG rmdir /tmp/$isoconf->{nameid}-$version-$isoconf->{arch}", $color);
	system("sudo rm -rf /tmp/$isoconf->{nameid}-$version-$isoconf->{arch}");
	print_color("$LOG rmdir $builddir", $color);
	#pathrm("$wd/$isoconf->{iso}{genisoimage}{builddir}/$name/$arch") or die $!;
        -d $builddir and system("rm -rf $builddir");
	print_color("$LOG rmdir $isodir", $color);
	#pathrm("$wd/$isoconf->{iso}{genisoimage}{isodir}/$name/$arch") or die $!;
	-d $isodir and system("rm -rf $isodir");
}
1;
