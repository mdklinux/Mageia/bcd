package BCD::Isolinux;

use File::Copy::Recursive qw(dircopy pathrm);
use BCD::Common qw(:DEFAULT $isoconf $wd $name $arch $version $based_on $repo $builddir);

our @ISA = qw(Exporter);
our @EXPORT = qw(main_isolinux);

my $LOG="ISOLINUX -";
my $color = "yellow";

my $repo_isolinux;
if (!defined($isoconf->{isolinux}{fullpath})) {
	$repo_isolinux = "$repo/$based_on/$arch/$isoconf->{isolinux}{defaultpath}/";
} else {
	$repo_isolinux = $isoconf->{isolinux}{fullpath};
}
my $build_isolinux_dir = "$builddir/isolinux";

sub switch_theme {
    my $theme = $isoconf->{theme}{bootsplash};
    print_color("$LOG switch to theme $theme", $color);
    # syslinux
    my $lilo_sys = "$isoconf->{theme}{bootsplash_path}/themes/$theme/lilo/syslinux";
    -f $lilo_sys and system("cp -vf $lilo_sys", "$build_isolinux_dir/boot.msg");
    print_color("$LOG copy $isoconf->{theme}{gfxboot_path}/themes/*.jpg to $build_isolinux_dir/",, $color);
    system("cp -vf $isoconf->{theme}{gfxboot_path}/themes/$theme/welcome.jpg $build_isolinux_dir/");
    system("cp -vf $isoconf->{theme}{gfxboot_path}/themes/$theme/back.jpg $build_isolinux_dir/");
}

sub add_entry {
    my $syslinuxcfg = "$build_isolinux_dir/isolinux.cfg";
    -f $syslinuxcfg or die "$LOG can't path $syslinuxcfg";
    foreach my $syslinux (@{$isoconf->{isolinux}{entry}}) {
	$syslinux->{name} or next;
	print_color("$LOG add $syslinux->{name} in $syslinuxcfg", $color);
	open my $SYS, ">> $syslinuxcfg";
	print $SYS "label $syslinux->{label}\n";
	print $SYS "    kernel $syslinux->{kernel}\n";
	print $SYS "    append $syslinux->{append}\n";
	close $SYS;
	if (-f $syslinux->{bin}) {
	       	system("cp -vf $syslinux->{bin} $build_isolinux_dir/");
	} else {
		die "$LOG $syslinux->{bin} is not present on the system !\n";
	}
    }
}

sub use_firmware {
    print_color("$LOG copy fw.gz to $build_isolinux_dir/alt0/", $color);
    my $syslinuxcfg = "$build_isolinux_dir/isolinux.cfg";
    open(IN, "+<$syslinuxcfg");
    @file = <IN>;
    seek IN,0,0;
    foreach (@file) {
	s|alt0/all.rdz|alt0/all.rdz,alt0/fw.gz|g;
        print IN $_;
    }
    close IN;
    system("cp -v /usr/lib*/drakx-installer-images/isolinux/alt0/fw.gz $build_isolinux_dir/alt0/");
}

sub copy_files {
	foreach my $file (@{$isoconf->{isolinux}{tocopy}}) {
		$file->{name} or next;
		if (-f $file->{file}) {
			print_color("$LOG copy $file->{file} in $build_isolinux_dir", $color);
			system("cp -avf $file->{file} $build_isolinux_dir/");
		} else {
			die "$LOG $file->{file} is not present on the system !\n";
		}
	}
}

sub main_isolinux {
    if (! -d $repo_isolinux) { 
	print_color("$LOG I can't find the isolinux dir $repo_isolinux !", $color);
	exit;
    }
    print_color("$LOG Remove old copy of isolinux", $color);
    pathrm("$build_isolinux_dir/isolinux") or die $!;
    print_color("$LOG copy $repo_isolinux to $build_isolinux_dir", $color);
    dircopy($repo_isolinux, $build_isolinux_dir) or die $!;
    add_entry;
    if ($isoconf->{isolinux}{firmware} eq "yes") {
	    use_firmware;
    }
    copy_files;
    switch_theme;
}

1;

