package BCD::Resign;

use strict;
use Parallel::ForkManager;
use File::Glob ':glob';
use File::Basename;
use Expect;

use BCD::Common qw(:DEFAULT $isoconf $NB_FORK $rpmrc $pwd_file $builddir);

our @ISA = qw(Exporter);
our @EXPORT = qw(resign_media);

my $verbose;
my $LOG="MEDIA -";
my $color = "yellow";

my $password;
if (defined $pwd_file) {
       -f $pwd_file and $password = `cat $pwd_file`;
}

sub print_info {
	map { if (!-f $pwd_file) { print_color("$LOG I cant find $_ file, i cant sign packages...", $color) } } $rpmrc, $pwd_file;
	print_color("$LOG i will resign using info in those files: $rpmrc $pwd_file", $color);
}

sub resign_media {
	print_info();
	my $pm = new Parallel::ForkManager($NB_FORK);
	my @list_path; my @checked; my $already_done;
	foreach my $media (@{$isoconf->{media}{list}}) {
	    	foreach (@checked) { $_ or next; $media->{destmedia} =~ /^$_$/ and $already_done = 1 }
	        $already_done and next;
		push @checked, $media->{destmedia};
		push @list_path, $media->{destmedia};
		print_color("$LOG resigning packages from $media->{destmedia} media", $color);
	}
	foreach (@list_path) {
	my @list_pkg = glob("$builddir/media/$_/*.rpm");
	my $count = @list_pkg;
	print_color("$count transactions to do ... be patient !!!!", $color);
	my $status = "0";
	foreach my $pkg (@list_pkg) {
		$pkg or next;
		my $basename_pkg = basename($pkg);
	        $status++;
	        my $pid = $pm->start and next;
	        print("$basename_pkg ($status/$count)\n");
	        my $command = Expect->spawn("LC_ALL=C rpm --rcfile=$rpmrc --resign $pkg") or die "Couldn't start rpm: $!\n";
	        $command->log_stdout($verbose);
	        $command->expect(20, '-re', 'Enter pass phrase:' => sub { print $command $password });
	        $command->expect(undef);
		$command->soft_close;
	        $pm->finish;
	}
	print_color("Waiting for the end of some signature...", $color);
	$pm->wait_all_children;
	print_color("all signature are done...", $color);
	}
}

1;
