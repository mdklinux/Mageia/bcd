package BCD::Genisoimage;

use strict;
use XML::Simple;
use BCD::Common qw(:DEFAULT $nameid $arch $isoconf $wd $name $arch $version $builddir $isodir $error_color $repo $based_on $support);
use BCD::Media qw(:DEFAULT parse_synthesis);
use URPM;

our @ISA = qw(Exporter);
our @EXPORT = qw(main_iso create_idx);
our @EXPORT_OK = qw($isoname);

my $LOG="GENISOIMAGE -";
my $color = "green";

our $isoname = $isoconf->{nameid} . '-' . $isoconf->{mediainfo}{version} . '-' . $isoconf->{arch} . '-' . $isoconf->{support};

sub create_version {
	# create the VERSION file available on the base of the ISO
    my $version_file = "$builddir/VERSION";
    print_color("$LOG create the $version_file file", $color);
    open my $VERSION, ">$version_file";
    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = gmtime(time());
    $year += 1900;
    my $tag = $isoconf->{iso}{tag};
    my $product = $isoconf->{iso}{product};
    my $publisher = $isoconf->{iso}{header}{publisherid};
    printf $VERSION "$publisher $name $tag %04d%02d%02d $hour:%02d\n", $year, $mon+1, $mday, $min;
    close $VERSION;
}

sub create_productid {
    print_color("$LOG create the $builddir/product.id", $color);
    my $productid_file = "$builddir/product.id";
    open my $product_id, ">$productid_file";
    print $product_id "vendor=$isoconf->{iso}{header}{publisherid},";
    print $product_id "distribution=$isoconf->{iso}{header}{systemid},";
    print $product_id "type=$isoconf->{iso}{type},";
    print $product_id "version=$isoconf->{based_on},";
    print $product_id "branch=$isoconf->{iso}{branch},";
    print $product_id "release=$isoconf->{iso}{minor}.$isoconf->{iso}{subversion},";
    print $product_id "arch=$arch,";
    print $product_id "product=$isoconf->{iso}{product}";
    close $product_id;
}

sub create_idx {
	print_color("$LOG create the IDX file from $builddir/pkg-$version-$isoconf->{iso}{tag}.idx", $color);
	# pkg-2009.1-pauillac.idx
	my $idx = "$builddir/pkg-$version-$isoconf->{iso}{tag}.idx";
	system("cp -vf  /dev/null $idx");
	use Data::Dumper;
	my $urpm = URPM->new;
	my @hdlists = glob("$builddir/media/media_info/hdlist_*.cz");
	my @tab;
	foreach (@hdlists) {
	print_color("$LOG parsing $_ hslist", $color);
	$urpm->parse_hdlist($_);
	$urpm->traverse(sub {
        	my $pkg  = shift;
		my $pkgname = $pkg->name;
		my $version = $pkg->version;
		my $arch = $pkg->arch;
		push @tab, "$pkgname-$version ($arch)";
	        }
	);
	}
	my %hashtab = map { $_ => 1 } @tab;
	my @orderedpkgs = sort keys %hashtab;
	open my $FILE_IDX, ">>$idx";
	foreach (@orderedpkgs) {
		print $FILE_IDX "$isoconf->{iso}{header}{volumeid} $_\n";
	}
	close $FILE_IDX;
	system("cp -v $builddir/pkg-$version-$isoconf->{iso}{tag}.idx $isodir/$isoname.idx");
}

sub create_iso {
        # build the ISO with all args
        # use -eltorito-alt-boot -e isolinux/efiboot.img -no-emul-boot for EFI
        print_color("$LOG Building the ISO", $color);

	if ($arch eq "x86_64") 	{
	my $cmd;
        my $cmd = "xorriso -as mkisofs $isoconf->{iso}{genisoimage}{options} \\
        -A '$isoconf->{iso}{header}{applicationid}' \\
        -publisher '$isoconf->{iso}{header}{publisherid}' \\
        -sysid '$isoconf->{iso}{header}{systemid}' \\
        -p '$isoconf->{iso}{header}{datapreparer}' \\
        -volset '$isoconf->{iso}{header}{volumesetid}' \\
        -V '$isoconf->{iso}{header}{volumeid}' \\
        -o '$isodir/$isoname.iso' \\
	-b isolinux/isolinux.bin  \\
	-boot-info-table -eltorito-alt-boot -e isolinux/efiboot.img -no-emul-boot \\
        $builddir/../
        ";
	} else 	{
	my $cmd;
	my $cmd = "xorriso -as mkisofs $isoconf->{iso}{genisoimage}{options} \\
        -A '$isoconf->{iso}{header}{applicationid}' \\
        -publisher '$isoconf->{iso}{header}{publisherid}' \\
        -sysid '$isoconf->{iso}{header}{systemid}' \\
        -p '$isoconf->{iso}{header}{datapreparer}' \\
        -volset '$isoconf->{iso}{header}{volumesetid}' \\
        -V '$isoconf->{iso}{header}{volumeid}' \\
        -o '$isodir/$isoname.iso' \\
        -b isolinux/isolinux.bin  \\
        -boot-info-table  \\
        $builddir/../
	";
	}
}
	
#-quiet \\
	print_color("\n------------\n $cmd", $color);
	system($cmd);
	system("du -h $isodir/$isoname.iso");
	system("du -sh $builddir/install");
	my $size_iso = -s "$isodir/$isoname.iso";
	if ($size_iso > 3900338176) {
		print_color("WARNING: THE SIZE OF THE ISO IS TOO BIG. PLEASE CHECK AGAIN.", $error_color);
	}

}

sub create_md5 {
	# create the MD5 of the ISO
	print_color("$LOG Create the MD5 and SHA1 of the ISO", $color);
	my $ISOFILE = "$isodir/$isoname.iso";
	if (! -f $ISOFILE) {
		print_color("$LOG can't find $ISOFILE", $error_color);
		exit 1;
	} else {
		print_color("$LOG do a: md5sum $ISOFILE > $ISOFILE.md5", $color);
		system("cd $isodir &&  md5sum $isoname.iso > $ISOFILE.md5 &");
		print_color("$LOG do a: sha1sum $ISOFILE > $ISOFILE.sha1", $color);
		system("cd $isodir &&  sha1sum $isoname.iso > $ISOFILE.sha1 &");
	}
}

sub isohybrid {
	print_color("$LOG isohybrid the ISO", $color);
	my $ISOFILE = "$isodir/$isoname.iso";
	system("isohybrid $isoconf->{iso}{isohybrid}{options} $ISOFILE") == 0 or die "$LOG system isohybrid failed: $?\n";
}

sub copy_textfiles {
	map { system("cp -vf $repo/$based_on/$arch/$_ $builddir/");
	} qw(COPYING INSTALL.txt install.htm LICENSE.txt LICENSE-APPS.txt README.txt index.htm release-notes.html release-notes.txt);
}

sub main_iso {
	my ($option) = @_;
	create_version;
	create_productid;
	copy_textfiles;
	create_idx;
	if ($option =~ /noiso/) {
		print_color("$LOG skipping ISO creation", $color);
	} else {
		create_iso;
		isohybrid;
		create_md5;
	}

}

1;

