package BCD::Bcd;

use strict;
use XML::Simple;
use Data::Dumper;
use Pod::Usage;
use MDK::Common;

use BCD::Resign;
use BCD::Common qw(:DEFAULT $wd $isoconf $name $arch $version $isodir $builddir $error_color $dir_deps);
use BCD::Genisoimage;
use BCD::Isolinux;
use BCD::Stagex;
use BCD::CheckMedia;
use BCD::Media;
use BCD::Web;

our @ISA = qw(Exporter);
our @EXPORT = qw(main_bcd);
our $conf_file;
my $color = "cyan";

sub print_info() {
	print Dumper($isoconf);
}

sub help_bcd {
	pod2usage('-verbose' => 2, '-input' => "bcd.pod", '-pathlist' => @INC);
}

sub do_all {
	my ($option) = @_;
	clean_all;
	check_dir;
	main_isolinux;
	main_stagex;
	main_media($option);
	main_iso($option);
	main_html;
	show_info();
}

sub show_info {
     print "\n";
     print "ISO dir: $isodir\n";
     print "build dir: $builddir\n";
     print "LOG of media: /tmp/$name-$version-$arch\n";
     print "stored URPMQ result: /var/lib/bcd/$name-$version-$arch\n";
     print "\n";
}


sub main_bcd() {
    my %opts = (
	'' => \&help_bcd,
	info => \&print_info,
	iso => \&main_iso,
	idx => \&create_idx,
	md5 => \&create_md5,
	stagex => \&main_stagex,
	isolinux => \&main_isolinux,
	media => \&main_media,
	gendistrib => \&use_gendistrib,
	clean => \&clean_all,
	list => \&list_media,
	mediarepo => \&list_medias_available,
	checkrepo => \&check_repo_hdlist,
        check => \&check_launch,
	doble => \&solve_doble_issue,
	kernel => \&find_all_kernel,
	resign => \&resign_media,
	getsrpm => \&get_srpms_list,
	clean => \&clean_urpmq_log,
	html => \&main_html,
	all => \&do_all,
	);
    


    if (! -d $dir_deps) {
	print_color(" I will create $dir_deps to store all urpmq queries", $error_color);
	print_color(" Please give full access RW to your current user to this directory", $color);
	mkdir_p($dir_deps);
    }
    print_color("-- If you don't want to use previous urpmq query:\n  ++ remove the directory $dir_deps\n  ++ or remove the file wich contains the urpmq result for the package to redo an another one (ie: the file $dir_deps/tocopy_plop_todo_Main-drakconf)", $color);
#    $ARGV[0] or help_bcd;
#    -f $ARGV[0] or die "Cant acces to conf file $ARGV[0]\n";
    if (my $f = $opts{$ARGV[1]}) {
	if ($ARGV[2]) {
		my $options;
		foreach my $arg (2 .. $#ARGV) {
		    $arg and $options = $options . ' ' . $ARGV[$arg];
	    	}
		$f->($options);
	} else {
		$f->();
	}
    } else {
	print_color(" ** Dont know what todo ** \n unknow option", $error_color);
    }
}


1;
