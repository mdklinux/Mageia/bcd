package BCD::CheckMedia;

use strict;
use MDK::Common;

use BCD::Common qw(:DEFAULT $isoconf $version $arch $repo $builddir $distrib_path $error_color $DISTRIB $THEME $rpmcheck_list_pkg $file_rpmcheck_log $file_smartcheck_log $smartcheck_list_pkg $smartcheck_dir $chroot_path);
use BCD::Media;

our @ISA = qw(Exporter);
our @EXPORT = qw(check_repo_hdlist rpmcheck_launch smartcheck_launch rpmcheck_list smartcheck_list check_launch);

my $LOG="CHECKMEDIA -";
my $color = "blue";

my $chroot_path = "/tmp/$isoconf->{nameid}-$version-$isoconf->{arch}";

sub rpmcheck_list {
    my @toget = cat_($rpmcheck_list_pkg);
    my $todo = @toget;
    if ($todo ne "0") {
	print_color("$LOG calculate deps from the auto-generated rpmcheck list file", $error_color);
	    my $list_filename = "$isoconf->{tocopy_file}_todo_rpmcheck";
	    my $list = "$chroot_path/urpmq/$list_filename";
	    my $urpmq_options = "--requires-recursive -c --sources $isoconf->{mediainfo}{urpmi_option}";
	    my $pkgs;
	    foreach (@toget) { $_ or next; chomp($_); $pkgs = "$_ " . $pkgs }
	    my $cmd_do_it = "LC_ALL=C sudo -P chroot $chroot_path urpmq --prefer $DISTRIB,$THEME $urpmq_options --no-recommends";
	    do_fork($pkgs, $list_filename, $list, $cmd_do_it);
    } else {
	print_color("$LOG No rpmcheck error", $error_color);
    }
}

sub smartcheck_list {
    my @toget = cat_($smartcheck_list_pkg);
    my $todo = @toget;
    if ($todo ne "0") {
	print_color("$LOG calculate deps from the auto-generated smart list file", $error_color);
	    my $list_filename = "$isoconf->{tocopy_file}_todo_smartcheck";
	    my $list = "$chroot_path/urpmq/$list_filename";
	    my $urpmq_options = "--requires-recursive -c --sources $isoconf->{mediainfo}{urpmi_option}";
	    my $pkgs;
	    foreach (@toget) { $_ or next; chomp($_); $pkgs = "$_ " . $pkgs }
	    my $cmd_do_it = "LC_ALL=C sudo -P chroot $chroot_path urpmq --prefer $DISTRIB,$THEME $urpmq_options --no-recommends";
	    do_fork($pkgs, $list_filename, $list, $cmd_do_it);
    } else {
	print_color("$LOG No smart check error", $error_color);
    }
}

sub rpmcheck_launch {
    my $options = "-explain -failures";
    my $destmedia = "0";
    print_color("$LOG launch rpmcheck to check hdlists", $color);
	foreach my $media (@{$isoconf->{media}{list}}) {
		$media->{name} or next;
 		$media->{drop} =~ /yes/ and BCD::Media::drop_media("$media->{name}-release");
		if ($destmedia eq $media->{destmedia}) { next }
		$destmedia = $media->{destmedia};
		system("cp $builddir/media/$destmedia/media_info/hdlist.cz $builddir/media/media_info/hdlist_$media->{name}.cz");
	}
    my $cmd = "zcat $builddir/media/media_info/hdlist*.cz | rpmcheck $options";
    print_color("$LOG $cmd", $color);
    system("$cmd 2> /dev/null > $file_rpmcheck_log") == 0 or die "$LOG system $cmd failed: $?\n";
    open my $LIST_FAILURE, ">$rpmcheck_list_pkg.tmp";
    my @list_failure = cat_($file_rpmcheck_log);
    foreach my $package (@list_failure) {
	    print $package;
          my ($package2) = $package =~ /^\s+(\S*)\s/;
          $package2 and print $LIST_FAILURE "$package2\n";
    }
    close $LIST_FAILURE;
    system("cat $rpmcheck_list_pkg.tmp | sort | uniq > $rpmcheck_list_pkg");
    print_color("$LOG check this file $file_rpmcheck_log", $color);
    print_color("$LOG check this file $rpmcheck_list_pkg", $color);
}

sub smartcheck_launch {
    my $options = "-o sync-urpmi-medialist=no --data-dir $smartcheck_dir";
    system("rm -rf $smartcheck_dir $smartcheck_list_pkg");
    my $destmedia = "0";
    my $MEDIAS; my $MEDIASWITHCOMMA;
    print_color("$LOG launch smart to check synthesys", $color);
    foreach my $media (@{$isoconf->{media}{list}}) {
	$media->{name} or next;
	$media->{drop} =~ /yes/ and BCD::Media::drop_media("$media->{name}-release");
	if ($destmedia eq $media->{destmedia}) { next }
	$destmedia = $media->{destmedia};
	$MEDIAS = $MEDIAS . " " . $media->{name};
	$MEDIASWITHCOMMA = $media->{name} . "," . $MEDIASWITHCOMMA;
	my $cmd = "smart channel --yes $options --add $media->{name} type=urpmi baseurl=$builddir/media/$media->{destmedia}/ hdlurl=media_info/synthesis.hdlist.cz";
	system($cmd);
    }
    system("smart update $options $MEDIAS");
    chop($MEDIASWITHCOMMA);
    my $cmd = "smart check $options --channels=$MEDIASWITHCOMMA 2>&1 | egrep -v '(Loading cache|Updating cache)' | awk -F 'requires' '{print \$2}' | sort | uniq  >> $smartcheck_list_pkg";
    system("du $smartcheck_list_pkg");
    print_color("$LOG $cmd", $color);
    system("$cmd > $file_smartcheck_log") == 0 or die "$LOG system $cmd failed: $?\n";
    open my $LIST_FAILURE, ">$smartcheck_list_pkg.tmp";
    my @list_failure = cat_($file_smartcheck_log);
    foreach my $package (@list_failure) {
	print $package;
	my ($package2) = $package =~ /^\s+(\S*)\s/;
	$package2 and print $LIST_FAILURE "$package2\n";
    }
    close $LIST_FAILURE;
    system("cat $smartcheck_list_pkg.tmp | sort | uniq > $smartcheck_list_pkg");
    print_color("$LOG check this file $file_smartcheck_log", $color);
    print_color("$LOG check this file $smartcheck_list_pkg", $color);
}

sub check_launch {
	if (!defined($isoconf->{checktool})) {
		print_color("$LOG BCD will use rpmcheck to verify hdlist. You can use choose between smart or rpmcheck, just add the option <checktool>smart/rpmcheck<checktool> in the XML configuration file", $color);
		rpmcheck_launch;
	} elsif ($isoconf->{checktool} =~ /rpmcheck/) {
		print_color("$LOG BCD will use rpmcheck to verify hdlist.", $color);
		rpmcheck_launch;
	} else {
		print_color("$LOG BCD will use smart check to verify hdlist.", $color);
		smartcheck_launch;
	}
}

sub check_repo_hdlist {
    my $options = "-explain -failures";
    print_color("$LOG check media repositery hdlists in $repo/$version/$arch", $color);
    system("zcat $repo/$version/$arch/media/media_info/hdlist*ain.cz | rpmcheck $options");
}

1;
