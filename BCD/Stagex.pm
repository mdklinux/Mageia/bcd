package BCD::Stagex;


use strict;
use XML::Simple;
use File::Copy::Recursive qw(dircopy pathrm);
use BCD::Common qw(:DEFAULT $isoconf $wd $name $arch $based_on $repo $version $builddir $error_color);

our @ISA = qw(Exporter);
our @EXPORT = qw(main_stagex);

my $LOG="STAGEX -";
my $color = "blue";
my $installer_destdir = "$builddir/$isoconf->{installer}{defaultpath}";

sub update_stage {
-d $repo or print_color("$LOG $repo does not exists !", $color) and exit;

my $installer_dir;
if (!defined($isoconf->{installer}{fullpath})) {
        $installer_dir = "$repo/$based_on/$arch/$isoconf->{installer}{defaultpath}";
} else {
	$installer_dir = "$isoconf->{installer}{fullpath}/install";
}
-d $installer_dir or print_color("$LOG $installer_dir does not exists !", $error_color) and exit;


#	print "$LOG remove old copy $builddir\n";
#	pathrm($builddir) or die $!;
	print_color("$LOG copy $installer_dir to $installer_destdir", $color);
	dircopy($installer_dir, $installer_destdir) or die $!;
}

sub update_advertising {
if (defined($isoconf->{installer}{advertising}{fullpath})) {
	print_color("$LOG copy $isoconf->{installer}{advertising}{fullpath} to $installer_destdir/extra/", $color);
	system("rm -rf $installer_destdir/extra/advertising");
	system("cp -a $isoconf->{installer}{advertising}{fullpath} $installer_destdir/extra/");
    }
}

sub copy_dosutils {
        print_color("$LOG copy of dosutils directory from $repo/$based_on/$arch to $builddir/../", $color);
        if (-d "$repo/$based_on/$arch/dosutils") {
                system("cp -a $repo/$based_on/$arch/dosutils $builddir/../");
                system("cp -a $repo/$based_on/$arch/autorun.inf $builddir/../");
        } else {
                print_color("$LOG FAILED copy of dosutils directory from $repo/$based_on/$arch", $error_color);
        }
}

sub copy_doc {
        print_color("$LOG copy of doc directory from $repo/$based_on/$arch to $builddir/", $color);
        if (-d "$repo/$based_on/$arch/doc") {
                system("cp -a $repo/$based_on/$arch/doc $builddir/");
        } else {
                print_color("$LOG FAILED copy of doc directory from $repo/$based_on/$arch", $error_color);
        }
}

sub copy_misc {
        print_color("$LOG copy of misc directory from $repo/$based_on/$arch to $builddir/../", $color);
        if (-d "$repo/$based_on/$arch/misc") {
		system("rm -rf $builddir/misc && mkdir $builddir/misc");
                system("cp -a $repo/$based_on/$arch/misc/drakx-in-chroot $builddir/misc/");
                system("cp -a $repo/$based_on/$arch/misc/mdkinst_stage2_tool $builddir/misc/");
        } else {
                print_color("$LOG FAILED copy of misc directory from $repo/$based_on/$arch", $error_color);
        }
}

sub patch_oem {
    if (-f $isoconf->{installer}{patch}) {
	print_color("$LOG copy $isoconf->{installer}{patch} to $installer_destdir/", $color);
	system("cp -a $isoconf->{installer}{patch} $installer_destdir/");
    }
}

sub main_stagex {
	update_stage;
	update_advertising;
	copy_dosutils;
	copy_doc;
	copy_misc;
	patch_oem;
}

1;
