#!/usr/bin/perl -w

use strict;
use Parallel::ForkManager;
use File::Glob ':glob';
use File::Basename;
use Expect;


my $NB_FORK=15;
# password file
my $pwd_file = "/home/plop/.signature.gpg";
my $rpmrc = "/home/plop/.rpmrc";
my $path = $ARGV[0];

$ARGV[0] or die "First arg must be a path to rpm\n";

my $password = `cat $pwd_file`;
my $verbose = "0" ;

my $pm = new Parallel::ForkManager($NB_FORK);
my @list_pkg = glob("$path/*.rpm");
my $count = @list_pkg;
print "$count transactions to do ... be patient !!!!";
my $status = "0";
foreach my $pkg (@list_pkg) {
	$pkg or next;
	my $basename_pkg = basename($pkg);
        $status++;
        my $pid = $pm->start and next;
        print("$basename_pkg ($status/$count)\n");
        my $command = Expect->spawn("LC_ALL=C rpm --rcfile=$rpmrc --resign $pkg") or die "Couldn't start rpm: $!\n";
        $command->log_stdout($verbose);
        $command->expect(20, -re, 'Enter pass phrase:' => sub { print $command $password; });
        $command->expect(undef);
	$command->soft_close();
        $pm->finish;
}
print "Waiting for the end of some signature...\n";
$pm->wait_all_children;
print "all signature are done...\n";
